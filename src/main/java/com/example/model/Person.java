package com.example.model;

import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Attribute;

@Entry(objectClasses = { "person" }, base = "ou=people")
public class Person {
    @Attribute(name = "cn")
    private String fullName;

    @Attribute(name = "mail")
    private String email;

    // 其他屬性和方法...
}