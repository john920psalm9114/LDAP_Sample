package com.example.model;

import java.util.List;

public class LdapQueryResp {

    private String userPassword;
    public String getUserPassword(){
        return this.userPassword;
    }
    public void setUserPassword(String userPassword){
        this.userPassword = userPassword;
    }

    private String mail;
    public String getMail(){
        return this.mail;
    }
    public void setMail(String mail){
        this.mail = mail;
    }
//
//    private String cn;
//    public String getCn(){
//        return this.cn;
//    }
//    public void setCn(String cn){
//        this.cn = cn;
//    }
//
//    private String sn;
//    public String getSn(){
//        return this.sn;
//    }
//    public void setSn(String sn){
//        this.sn = sn;
//    }
//
//    private List<String> objectClassList;
//    public List<String> getObjectClassList(){
//        return this.objectClassList;
//    }
//    public void setObjectClassList(List<String> objectClassList){
//        this.objectClassList = objectClassList;
//    }

}
