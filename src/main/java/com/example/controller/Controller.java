package com.example.controller;;

import java.util.*;
import com.example.model.LdapQueryReq;
import com.example.model.LdapQueryResp;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.NamingException;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.ldap.core.LdapTemplate;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import org.springframework.ldap.query.LdapQueryBuilder;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class Controller {

    @Value("${spring.ldap.url}")
    private String url;
    @Value("${spring.ldap.base}")
    private String base;
    @Value("${spring.ldap.username}")
    private String username;
    @Value("${spring.ldap.password}")
    private String password;
    private LdapQueryBuilder ldapQueryBuilder;

    public LdapTemplate initializeLdapTemplate() {
        LdapContextSource contextSource = new LdapContextSource();
        contextSource.setUrl(url);
        contextSource.setBase(base);
        contextSource.setUserDn(username);
        contextSource.setPassword(password);
        contextSource.afterPropertiesSet();
        return new LdapTemplate(contextSource);
    }

    // ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ | POST | /person | ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓ ↓
    @PostMapping("/person")
    public ResponseEntity<List<LdapQueryResp>> postPerson(@RequestBody LdapQueryReq request) {
        Date date = new Date();
        System.out.println(date);
        LdapTemplate ldapTemplate = initializeLdapTemplate();

        String uid = request.getUid();

        List<LdapQueryResp> ldapQueryList = ldapTemplate.search(
                ldapQueryBuilder.query().where("uid").is(uid),
                new AttributesMapper<LdapQueryResp>() {
                    public LdapQueryResp mapFromAttributes(Attributes attrs) throws NamingException, javax.naming.NamingException {
                        LdapQueryResp ldapQueryResp = new LdapQueryResp();

                        if (attrs.get("userPassword") != null) {
                            byte[] passwordBytes = (byte[]) attrs.get("userPassword").get();
                            String password = new String(passwordBytes);
                            ldapQueryResp.setUserPassword(password);
                        }

                        if (attrs.get("mail") != null) {
                            ldapQueryResp.setMail(attrs.get("mail").get().toString());
                        }

//                        if (attrs.get("cn") != null) {
//                            ldapQueryResp.setCn(attrs.get("cn").get().toString());
//                        }
//                        if (attrs.get("sn") != null) {
//                            ldapQueryResp.setSn(attrs.get("sn").get().toString());
//                        }
//
//                        if (attrs.get("objectClass") != null) {
//                            Attribute objectClassAttr = attrs.get("objectClass");
//                            NamingEnumeration<?> objectClasses = objectClassAttr.getAll();
//                            List<String> objectClassList = new ArrayList<>();
//                            while (objectClasses.hasMore()) {
//                                objectClassList.add(objectClasses.next().toString());
//                            }
//                            ldapQueryResp.setObjectClassList(objectClassList);
//                        }

                        return ldapQueryResp;
                    }
                });

        for (LdapQueryResp ldapTemplate1 : ldapQueryList) {
            System.out.println("+++userPassword:" + ldapTemplate1.getUserPassword());
            System.out.println("+++mail:" + ldapTemplate1.getMail());
//            System.out.println("+++cn:" + ldapTemplate1.getCn());
//            System.out.println("+++sn:" + ldapTemplate1.getSn());
//            System.out.println("+++objectClass:" + ldapTemplate1.getObjectClassList());
        }

        return ResponseEntity.ok().body(ldapQueryList);
    }
    // ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ | POST | /person | ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑ ↑

}